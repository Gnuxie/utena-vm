;;;; package.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(defpackage #:utena-vm.standard-machine
  (:use #:cl)
  (:local-nicknames
   (#:o #:utena-vm.objects)
   (#:m #:utena-vm.machine)
   (#:so #:utena-vm.standard-objects))
  (:export
   ;; constants
   #:+op-literal+
   #:+op-arg-count+
   #:+op-send+
   #:+op-implicit-self-send+
   #:+op-reset+
   #:+op-escape+
   #:+op-activation+
   #:make-instruction

   #:make-machine

   #:activate
   #:make-activation
   #:evaluate-activation-record

   ))
