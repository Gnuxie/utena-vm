#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:utena-vm.machine
  (:use #:cl)
  (:export
   #:abstract-machine
   #:push-execution-stack
   #:pop-execution-stack
   #:send-message
   #:evaluate-message
   #:pop-n
   #:active-activation-object))

(in-package #:utena-vm.machine)

(defclass abstract-machine () ())

(defgeneric push-execution-stack (machine value)
  (:documentation "push a value onto the execution stack."))
(defgeneric send-message (object message-key machine &rest arguments)
  (:documentation "Send a message to the object with the selector and arguments provided.

This is a utility used by the primitive methods to send messages in the same way
the guest environment does."))
(defgeneric evaluate-message (lookup-value object machine)
  (:documentation "Evaluate the message that was sent,
the lookup-value is usually a slot, so for example if you were creating the data slot
you'd want to define behaviour to push the value onto the execution stack here."))
(defgeneric pop-n (machine n)
  (:documentation "pop n arguments from the execution stack."))
(defgeneric pop-execution-stack (machine)
  (:documentation "pop one argument off the execution stack."))
(defgeneric active-activation-object (machine)
  (:documentation "return the actively executing activation record"))
