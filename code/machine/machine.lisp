#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-machine)

;;; the return record needs to be a slot on an activiation record.
;;; so much state belongs on the activation record
;;; rather than the VM.
(defclass standard-machine (m:abstract-machine standard-dynamic-environment)
  ((%execution-stack :initarg :execution-stack :accessor execution-stack
                     :initform '() :type list)

   (%active-record :initarg :active-record :accessor active-record
                   :initform nil)
   (%argument-count-register :initarg :argument-count-register
                             :accessor argument-count-register
                             :type (unsigned-byte) :initform 0)))
(defmethod m:active-activation-object ((machine standard-machine))
  (activation-self (active-record machine)))

;;; https://www.scheme.com/tspl4/control.html#./control:h6
(defmethod m:evaluate-message ((slot o:data-slot-description)
                               object (machine standard-machine))
  (let ((slot-value (o:data-slot-value slot object)))
    (typecase slot-value
      (activation-record
       (m:push-execution-stack machine slot-value))
      (o:method
       (let ((activation (activate machine object slot-value)))
         (evaluate-activation-record
          machine
          activation
          (o:method-code slot-value))))
      (t (m:push-execution-stack machine slot-value)))))

(defmethod m:evaluate-message ((slot o:assignment-slot-description)
                               object (machine standard-machine))
  (setf (o:data-slot-value (o:target-slot-description slot) object)
        (m:pop-execution-stack machine)))

(defmethod m:evaluate-message ((slot o:foreign-method-slot)
                               object (machine standard-machine))
  (let* ((foreign-method (o:data-slot-value slot object))
         (arguments
           (loop :for argument-slot :across (o:method-arguments foreign-method)
                 :if (typep argument-slot 'so::direct-argument-slot)
                   :collect (m:pop-execution-stack machine)
                 :if (typep argument-slot 'so::rest-argument-slot)
                   :append (m:pop-n machine (argument-count-register machine))))
         (result
           (apply #'o:foreign-method-call foreign-method machine object
                  arguments)))
    (unless (null result)
      (m:push-execution-stack machine result))))

(defmethod m:pop-n ((machine standard-machine) n)
  (let* ((head (execution-stack machine))
         (nth-cons (nthcdr (1- n) head)))
    (setf (execution-stack machine)
          (cdr nth-cons))
    (when (cdr nth-cons)
      (setf (cdr nth-cons) nil))
    head))

(defmethod m:pop-execution-stack ((machine standard-machine))
  (pop (execution-stack machine)))

(defmethod m:push-execution-stack ((machine standard-machine) value)
  (push value (execution-stack machine)))

(defun make-machine ()
  (make-instance 'standard-machine))

(defmethod dynamic-evaluate ((catch-entry catch-entry) (machine standard-machine))
  (dynamic-unwind machine catch-entry)
  (let ((next-activation (activation-record catch-entry)))
    (setf (program-counter next-activation)
          (continue-point next-activation))
    (setf (active-record machine)
          next-activation)))

(defgeneric dynamic-unwind (machine exit-point-entry)
  (:method ((machine standard-machine) exit-point-entry)
;;; first step is to wind up until the entry
;;; if we come across an unwind-protect-entry, invoke it
;;; then door's open.
    (loop :for record := (first (environment-head machine))
          ;; for both reset and escape, we can target the escape record.
          :until (or (null record)
                     (eq (activation-record record)
                         (escape-record exit-point-entry)))
          :when (typep record 'unwind-protect-entry)
          :do (error "unimplemented: got a unwind-protect entry ~a" record)
          :when (typep record 'exit-point-entry)
          :do (invalidate record)
          :do (pop (environment-head machine)))))
