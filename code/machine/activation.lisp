#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-machine)

;;; TODO
;;; when we save registers are we going to be saving registers
;;; from the machine or from the actication record
;;; it seems to me like we've spent a lot of time
;;; creating registers on activctaion records
;;; i think we should stick with SICL
;;; and have the dynamic environment on the machine at the least
(defclass abstract-activation ()
  ((%activated-method :initarg :activated-method :reader activated-method)
   (%program-counter :initarg :program-counter :accessor program-counter
                     :type (unsigned-byte) :initform 0)
   (%escape-record :initarg :escape-record :reader escape-record
                   :initform nil
                   :documentation "This is the record
which called this one, can be nil if there wasn't one")))

(defclass activation-record (abstract-activation so:method-block
                             dynamic-environment-entry)
  ((%activation-self :initarg :activation-self :reader activation-self
                     :documentation "This is the object that the method is
being called on.")
   (%validp :initarg :validp :accessor validp
            :initform t :type boolean)))

;;; note, this only invalidates the ar in the dynamic environment
;;; the lexical environment of an activation record can outlive
;;; its dynamic extent when a closure has been created and returned.
(defmethod invalidate ((entry activation-record))
  (setf (validp entry) nil)
  entry)

(defmethod activation-record ((activation-record activation-record))
  activation-record)

(defun add-self* (activation-record self)
  (o:add-slot activation-record
              (o:make-slot :slot-parent-p t
                           :slot-key (o:internal-symbol "self*")
                           :slot-assignable-p nil)
              self)
  activation-record)

;;; TODO
;;; we need to think about calling methods form inside primitive methods next
;;; this will require consulting whether we need a specific method slot.
(defun activate (machine self method)
  (let* ((argument-descriptions (o:method-arguments method))
         (activation-object (o:clone method))
         (activation-record
           (change-class activation-object
                         'activation-record
                         :activated-method method
                         :activation-self self
                         :escape-record (active-record machine))))
    (loop :for argument-slot :across argument-descriptions
          :for execution-argument := (m:pop-execution-stack machine)
          :do (setf (o:data-slot-value activation-object argument-slot)
                    execution-argument))
    ;; technically the method should have this slot 'penciled in'
    ;; and it should be assignable.
    (add-self* activation-record self)
    activation-record))
