;;;; utena-vm.machine.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:utena-vm.machine.protocol
  :description "Describe utena-vm.machine here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "COOPERATIVE SOFTWARE LICENSE"
  :version "0.0.1"
  :serial t
  :components ((:file "protocol")))
