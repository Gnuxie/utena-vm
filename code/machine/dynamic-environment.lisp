#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-machine)

(defclass standard-dynamic-environment ()
  ((%environment-head :initarg :environment-head
                      :accessor environment-head
                      :type list :initform '())))

(defgeneric dynamic-lookup (environment message-selector)
  (:method ((environment standard-dynamic-environment) message-selector)
    (find message-selector (environment-head environment))))

(defgeneric dynamic-evaluate (dynamic-entry environment)
  (:documentation "This generic is used by the dynamic send instruction
after a message has been evaluated."))

(defgeneric dynamic-push (dynamic-entry environment)
  (:method ((environment standard-dynamic-environment) entry)
    (push entry (environment-head environment))))

;;; We are copying these basically straight from the SICL
;;; specification.
(defclass dynamic-environment-entry ()
  ())

(defgeneric invalidate (entry))

;;; TODO
;;; what sort of code or object are we going to put
;;; as the so called 'cleanup thunk' from SICL??
(defclass unwind-protect-entry (dynamic-environment-entry)
  ())

(defclass recieving-entry (dynamic-environment-entry)
  ((%message-selector :initarg :message-selector
                      :reader message-selector)))

(defclass exit-point-entry ()
  ((%activation-record :initarg :activation-record
                       :reader activation-record)
   (%continue-point :initarg :continue-point
                    :reader continue-point
                    :documentation "This is the integer to set
the program counter of the activation record to in order to continue execution")

   (%validp :initarg :validp :reader validp
             :initform t :type boolean)))

(defmethod invalidate (exit-point-entry)
  (setf (slot-value exit-point-entry '%validp) nil))

;;; I don't know if there's any need to have method calls
(defclass special-binding-entry (dynamic-environment-entry)
  ())

(defclass data-special-binding-entry (recieving-entry special-binding-entry)
  ((%message-value :initarg :message-value
                   :reader message-value)))

(defun make-special-binding-entry (selector value)
  (make-instance 'data-special-binding-entry
                 :message-selector selector
                 :message-value value))

(defclass catch-tag (exit-point-entry so:standard-object)
  ()
  (:documentation "This is pushed onto the stack as well as becoming
and entry in the dynamic environment. When the stack unwinds
the entry in the environment is invalidated so that you can't
unwind to an activation record that has been killed."))

(defmethod o:clone ((catch-tag catch-tag))
  (error "Cloning this tag is not allowed, do not expose to the VM."))

(defclass catch-entry (exit-point-entry)
  ())

;;; TODO
;;; do we want special bindings to behave like a message send?
;;; is the dynamic environment supposed to behave like an object?
;;; i'd say it should do!
;;; but then we need to see if we can use the protocol
;;; technically we only need to implement a lookup-message method

;;; the dynamic environment needs to be accessible via a mirror
;;; when you do setf in the dynamic environment it will be able
;;; to introduce a new binding at point
;;; this will be a new primitie that is made avaialble by the VM
;;; but this means that we need to have our protocol for creating
;;; primitives sorted out, so that it can be used from the standard
;;; machine package.
