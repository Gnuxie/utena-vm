#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-machine)

(alexandria:define-constant +op-self+ 1)
(alexandria:define-constant +op-literal+ 2)
(alexandria:define-constant +op-send+ 5)
(alexandria:define-constant +op-implicit-self-send+ 6)
(alexandria:define-constant +op-arg-count+ 8)
(alexandria:define-constant +op-activation+ 9)
(alexandria:define-constant +op-dynamic-send+ 11)
(alexandria:define-constant +op-reset+ 14)
(alexandria:define-constant +op-escape+ 15)

(declaim (inline opcode))
(defun opcode (instruction)
  (declare (type (unsigned-byte 16) instruction ))
  (ldb (byte 4 12) instruction))

(declaim (inline operand))
(defun operand (instruction)
  (declare (type (unsigned-byte 32) instruction))
  (ldb (byte 12 0) instruction))

(declaim (inline make-instruction))
(defun make-instruction (opcode operand)
  (setf (ldb (byte 4 12) operand)
        opcode)
  operand)

(declaim (inline reciever))
(defun reciever (execution-stack)
  (first execution-stack))

(defun op-self (machine activation)
  (m:push-execution-stack machine (activation-self activation)))

(defun op-literal (machine activation literal-index)
  (m:push-execution-stack machine
                          (aref (o:method-literals (activated-method activation))
                                literal-index)))

(defun %send (reciever machine activation selector-index)
  (let* ((selector
           (aref (o:method-literals (activated-method activation))
                 selector-index)))
    (multiple-value-bind (slot object) (o:lookup-message reciever selector)
      (assert (not (null slot)))
      (m:evaluate-message slot object machine))))

(defun op-send (machine activation selector-index)
  (let ((reciever (m:pop-execution-stack machine)))
    (%send reciever machine activation selector-index)))

(defun op-implicit-self-send (machine activation selector-index)
  (%send activation machine activation selector-index))

;;; This is used to use message send from primitives.
;;; this is a bit hacky to be fair.
(defmethod m:send-message (object message-key (machine standard-machine)
                           &rest arguments)
  (dolist (arg (reverse arguments))
    (m:push-execution-stack machine arg))
  (let ((slot (o:lookup-message object message-key)))
    (m:evaluate-message slot object machine)))

(declaim (inline op-arg-count))
(defun op-arg-count (machine count)
  "The bytecodes are explicitly unboxed values at the moment
so it would be pretty unwise to be pushing unboxed values
onto the execution stack, where they could be accessed from the VM."
  (setf (argument-count-register machine)
        count))

(declaim (inline op-activation))
(defun op-activation (machine)
  (dynamic-push machine (active-record machine))
  (m:push-execution-stack machine (active-record machine)))

(define-condition non-local-transfer-of-control
    ()
  ((%activation-record :initarg :activation-record
                       :reader activation-record)))

(defun transfer-control (activation-record)
  (signal 'non-local-transfer-of-control
          :activation-record activation-record))

(declaim (inline op-unwind))
(defun op-escape (machine)
  (let ((record (m:pop-execution-stack machine)))
    (assert (validp record))
    (dynamic-unwind machine record)
    (let ((escape-record (escape-record record)))
      (assert (not (null escape-record)))
      ;; this would cause an infinite loop otherwise
      ;; and we want to return to the point after.
      (incf (program-counter escape-record))
      (transfer-control escape-record))))

(declaim (inline op-reset))
(defun op-reset (machine)
  (let ((record (m:pop-execution-stack machine)))
    (assert (validp record))
    (dynamic-unwind machine record)
    (setf (program-counter record) 0)
    (setf (active-record machine) (escape-record record))
    (transfer-control record)))


(defun op-dynamic-send (machine activation message-selector-index)
  (let* ((message-selector
           (aref (o:method-literals (activated-method activation))
                 message-selector-index))
         (lookup-value (dynamic-lookup machine message-selector)))
;;; so there are two possibilites here
;;; one is that the value is some special dynamic environment entry
;;; the other is that it's some generic thing that already exists
    (dynamic-evaluate lookup-value machine)))

(defun evaluate-instruction (machine activation instruction)
  (let ((opcode (opcode instruction))
        (operand (operand instruction)))
    (ecase opcode
      (#.+op-self+ (op-self machine activation))
      (#.+op-literal+ (op-literal machine activation operand))
      (#.+op-send+ (op-send machine activation operand))
      (#.+op-implicit-self-send+ (op-implicit-self-send machine activation operand))
      (#.+op-arg-count+ (op-arg-count machine operand))
      (#.+op-activation+ (op-activation machine))
      (#.+op-escape+ (op-escape machine))
      (#.+op-reset+ (op-reset machine)))
    (incf (program-counter activation))))

(declaim (inline %evaluate-activation-record))
(defun %evaluate-activation-record (machine activation code)
  (let ((old-record (active-record machine)))
    (setf (active-record machine) activation)
    (loop :while (< (program-counter activation) (length code))
          :for instruction := (aref code (program-counter activation))
          :do (evaluate-instruction machine activation instruction))
    (setf (active-record machine) old-record))  )

(defun evaluate-activation-record (machine activation code)
  (let ((activation activation)
        (code code))
    (tagbody :start
       (handler-bind ((non-local-transfer-of-control
                        (lambda (c)
                          (when (eq activation (activation-record c))
                            (setf (active-record machine)
                                  (activation-record c))
                            (go :start)))))
         (%evaluate-activation-record machine activation code)))))
