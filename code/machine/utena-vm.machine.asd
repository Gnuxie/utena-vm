;;;; utena-vm.machine.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:utena-vm.machine
  :description "Describe utena-vm.machine here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "COOPERATIVE SOFTWARE LICENSE"
  :version "0.0.1"
  :depends-on ("utena-vm.machine.protocol" "utena-vm.objects")
  :serial t
  :components ((:file "package")
               (:file "dynamic-environment")
               (:file "machine")
               (:file "activation")
               (:file "interpreter")))
