#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

;;; transition tree has add-slot and remove-slot
;;; that should be it i think.
;;; remove slot complicates things as it means
;;; we have to keep parents of node0s.

(defclass standard-map (o:map)
  ((%slot-descriptions :initarg :slot-descriptions :reader slot-descriptions
                       :initform '() :type list)
   (%transitions :initarg :transitions :accessor transitions
                 :type list :initform '())))

;;; here's where you admit that you dunno much about tree theory
;;; and dunno how to write this iteratively but
;;; but it's not really an issue
(defun %walk-parent-objects (object selector visited-objects)
  (if (member object visited-objects)
      (values nil (cons object visited-objects))
      (let ((local-slot (o:find-slot (o:object-map object) selector)))
        (push object visited-objects)
        (cond
          (local-slot
           (values (list (cons local-slot object))
                   visited-objects))

          (t (let ((slots '()))
               (dolist (parent (o:parent-slots (o:object-map object)))
                 (multiple-value-bind (slot-set next-visited-objects)
                     (%walk-parent-objects (o:slot-location parent)
                                           selector
                                           visited-objects)
                   (setf visited-objects
                         next-visited-objects)
                   (unless (null slot-set)
                     (setf slots
                           (append slot-set slots)))))
               (values slots visited-objects)))))))

(defmethod o:lookup-message ((object o:object) selector)
  (macrolet ((slot (cons) `(car ,cons))
             (object (cons) `(cdr ,cons)))
    (let ((slots (%walk-parent-objects object selector nil)))
      ;; ambiguos mesage send should be raised
      (assert (>= 1 (length slots)))
      (let ((result (first slots)))
        (values (slot result) (object result))))))

(defmethod o:parent-slots ((map standard-map))
  (remove-if-not #'o:slot-parent-p (slot-descriptions map)))

(defmethod o:find-slot ((map standard-map) selector)
  (find selector (slot-descriptions map) :key #'o:slot-key))

(defgeneric find-transition (transition-node descriptor)
  (:documentation "Used by transition to find a map if the description has already been added to this map before.")
  (:method ((transition-node standard-map) descriptor)
    (cdr (assoc descriptor (transitions transition-node)))))

(defgeneric transition (map descriptor)
  (:documentation "Provide a new map containing the provided slot description")
  (:method ((map standard-map) descriptor)
    (let ((existing-map (find-transition map descriptor)))
      (or existing-map
          (let ((next-map
                  (make-instance 'standard-map
                                 :slot-descriptions
                                 (cons descriptor
                                       (slot-descriptions map)))))
            (setf (transitions map)
                  (acons descriptor next-map (transitions map)))
            next-map)))))

(defclass standard-slot-description (o:data-slot-description)
  ((%slot-key :initarg :slot-key :reader o:slot-key)
   (%slot-location :initarg :slot-location :accessor o:slot-location)
   (%slot-assignable-p :initarg :slot-assignable-p :reader o:slot-assignable-p
                       :type boolean :initform nil)
   (%slot-parent-p :initarg :slot-parent-p :reader slot-parent-p
                   :reader o:slot-parent-p
                   :type boolean :initform nil)))

(defclass standard-object (o:mapped-object)
  ((%object-map :initarg :object-map :reader o:object-map
                :accessor %object-map :initform (make-instance 'standard-map))
   (%object-vector :initarg :object-vector :reader object-vector
                   :initform (make-array 3 :fill-pointer 0 :adjustable t))))

(defmethod o:object-ref ((object standard-object) ref)
  (aref (object-vector object) ref))

(defmethod (setf o:object-ref) (new (object standard-object) ref)
  (setf (aref (object-vector object) ref) new))

(defmethod o:add-slot ((object standard-object) slot-description value)
  (cond
    ((o:slot-assignable-p slot-description)
     (setf (o:slot-location slot-description) (fill-pointer (object-vector object)))
     (vector-push-extend value (object-vector object)))
    (t (setf (o:slot-location slot-description) value)))
  (setf (%object-map object)
        (transition (o:object-map object) slot-description))
  slot-description)

(defmethod o:add-assignment-slot ((object standard-object) target-slot-description selector)
  (assert (o:slot-assignable-p target-slot-description))
  (let ((assignment-slot-description
          (make-instance 'standard-assignment-slot
                         :target-data-slot target-slot-description
                         :slot-key selector)))
    (setf (%object-map object)
          (transition (o:object-map object) assignment-slot-description))
    assignment-slot-description))

(defmethod o:make-object (&key slot-descriptions initial-values)
  (assert (= (length slot-descriptions)
             (length initial-values))
          () "there is an unequal number of initial-values for slot-descriptions")
  (let ((object (make-instance 'standard-object)))
    (loop :for slot :in slot-descriptions
          :for value :in initial-values
          :do (o:add-slot object slot value))
    object))

(defmethod o:make-slot (&rest initargs)
  (apply #'make-instance 'standard-slot-description initargs))

(defmethod o:clone ((object standard-object))
  (make-instance 'standard-object
                 :object-map (%object-map object)
                 :object-vector (alexandria:copy-array (object-vector object))))

;;;; box -----------------------------------------------------------------------

(alexandria:define-constant +hidden-parent-selector+ '+hidden-parent-selector+)

;;; if we need a box that doesn't wrapp anything later on
;;; then just make another class.

;;; TODO, sorta critical
;;; wrapped-object actaully needs to traverse the prototype chain
;;; like lookup-message, but instead it's looking for an object
;;; which is a box, when there are more than one boxes in the chain
;;; then we need to provide an ambigous message send or soemthing.
;;; the same applies for finding the box Self, not the contextual self.
;;; See issue on the future of boxing (21).
(defclass standard-box (o:box)
  ((%object-map :initarg :object-map
                :accessor o:object-map
                :allocation :class
                :initform (make-instance 'standard-map))
   (%wrapped-object :initarg :wrapped-object
                    :reader o:wrapped-object)))

(defmacro define-primitive (box-name
                            (&key additional-reader wrapped-slot-type
                               wrapped-initform))
  `(progn
     (defclass ,box-name (standard-box)
       ;; CLHS defclass, if we specify the slot again
       ;; then the allocation will be not be shared with the superclass.
       ,(list* '(%object-map :allocation :class)
         (if (or additional-reader wrapped-slot-type wrapped-initform)
             `(,(append '(%wrapped-object)
                        (when additional-reader
                          `(:reader ,additional-reader))
                        (when wrapped-slot-type
                          `(:type ,wrapped-slot-type))
                        (when wrapped-initform
                          `(:initform ,wrapped-initform))))
             nil)))))

(defmethod o:upgrade-box ((box standard-box))
  (let ((wrapped-object (o:wrapped-object box)))
    (change-class box 'standard-object)
    (let ((forwarding-parent-slot
            (o:make-slot :slot-parent-p t
                         :assignable-p nil
                         :slot-key +hidden-parent-selector+)))
      (o:add-slot box forwarding-parent-slot
                  (o:box-primitive wrapped-object))))
  box)

(defmethod o:add-slot ((object standard-box) slot-description value)
  (o:upgrade-box object)
  (o:add-slot object slot-description value))

;;; maybe primitive needs to decide how to do this
(defmethod o:clone ((object standard-box))
  object)

(defun %primitive-method-description (selector method)
  (make-instance 'standard-foreign-method-slot
                 :slot-key selector
                 :slot-assignable-p nil
                 :slot-location method))

(defgeneric add-primitive-method (object selector function)
  (:method ((object standard-box) selector  function)
    (let ((slot-description (%primitive-method-description selector function)))
      (let ((map (o:object-map object)))
        (setf (slot-value map '%slot-descriptions)
              (cons slot-description
                    (slot-value map '%slot-descriptions))))
      object))

  (:method ((object standard-object) selector function)
    (let ((slot-description (%primitive-method-description selector function)))
      (o:add-slot object slot-description function))))

(defmethod add-primitive-method ((class-name symbol) (selector string) method)
  (let ((dummy (make-instance class-name))
        (symbol (o:internal-symbol selector)))
    (add-primitive-method dummy symbol method)))
