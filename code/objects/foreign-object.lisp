#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.objects)

;;; this is silly, don't do it until we have to
;;; use the normal object, but put foreign objects inside it

#+ (or)
(defclass foreign-object (object)
  ((%parent :initarg :parent :reader parent
            :initform nil
            :documentation "Only one parent
so that we don't have to do a lot of mop work.")
   (%utena-slots :intarg :utena-slots :reader utena-slots
                 :initform '()  :type list)))
