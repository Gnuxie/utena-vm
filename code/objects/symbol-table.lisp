#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(define-primitive standard-symbol
    (:wrapped-slot-type string))

(defvar o:*internal-symbol-table* nil)

(define-primitive symbol-table
    (:additional-reader table
     :wrapped-slot-type hash-table
     :wrapped-initform (make-hash-table :test 'equal)))

(or o:*internal-symbol-table*
    (setf o:*internal-symbol-table* (make-instance 'symbol-table)))

(defun make-symbol (name)
  (make-instance 'standard-symbol :wrapped-object name))

(defun o:internal-symbol (name)
  (o:intern o:*internal-symbol-table* name))

(defgeneric o:find-symbol (symbol-table symbol)
  (:method ((symbol-table symbol-table) (symbol string))
    (gethash symbol (table symbol-table))))

(defgeneric o:intern (symbol-table name)
  (:method ((symbol-table symbol-table) (name string))
    (or (o:find-symbol symbol-table name)
        (setf (gethash name (table symbol-table))
              (make-instance 'standard-symbol :wrapped-object name)))))
