#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(defclass standard-assignment-slot (o:assignment-slot-description)
  ((%target-data-slot :initarg :target-data-slot
                      :reader o:target-slot-description)
   (%slot-key :initarg :slot-key :reader o:slot-key)
   (%slot-assignable-p :reader o:slot-assignable-p
                       :allocation :class :type boolean :initform nil)))

(defmethod o:slot-parent-p ((standard-assignment-slot standard-assignment-slot))
  nil)
