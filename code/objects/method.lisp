#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(defclass standard-method (method-block)
  ())

(defun o:make-method (&rest initargs)
  (apply #'make-instance 'standard-method initargs))
