#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(define-primitive primitive-boolean
    (:wrapped-slot-type boolean
     :wrapped-initform nil))

(defun %activate-block (block machine)
  (u-vm.m:send-message block (o:internal-symbol "value") machine nil))

(define-box-method "if-true" (primitive-boolean) (true-block)
  (when (o:wrapped-object self)
    (%activate-block true-block machine))
  nil)

(define-box-method "if-false" (primitive-boolean) (true-block)
  (unless (o:wrapped-object self)
    (%activate-block true-block machine))
  nil)

(defmethod o:box-primitive ((true-value (eql t)))
  (make-instance 'primitive-boolean :wrapped-object true-value))

(defmethod o:box-primitive ((false-value (eql nil)))
  (make-instance 'primitive-boolean :wrapped-object false-value))
