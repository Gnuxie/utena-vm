;;;; utena-vm.objects.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:utena-vm.objects
  :description "Describe utena-vm.objects here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "COOPERATIVE SOFTWARE LICENSE"
  :version "0.0.1"
  :depends-on ("utena-vm.machine.protocol" "alexandria")
  :serial t
  :components ((:file "package")
               (:file "protocol")
               (:file "assignment-slot")
               (:file "object")
               (:file "method-block")
               (:file "method")
               (:file "foreign-method")
               (:file "symbol-table")
               (:file "symbol-table-prim")
               (:file "block")
               (:file "boolean")
               (:file "string")
               (:file "number")))
