#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(defclass standard-foreign-method (o:foreign-method standard-box)
  ((%function :initarg :wrapped-object
              :reader o:wrapped-object)
   (%arguments :initarg :method-arguments :reader o:method-arguments
               :documentation "The arguments that the foreign method
accepts, probably needs to be generated but it is required unfortunatley
as we can't have two calling conventions.")))

(defun method-arguments<-cl-lambda-list (lambda-list)
  (loop :named argument-builder
        :for symbol :in lambda-list
        :if (eql symbol '&rest)
        :collect (make-rest-argument-slot) :into arguments
        :and :do (return-from argument-builder arguments)
        :else :collect (make-argument-slot
                        :slot-key (o:internal-symbol (symbol-name symbol)))))

(defun lay-foreign-function-constructor (machine-var self-var arguments body)
  `(make-instance 'standard-foreign-method
                  :wrapped-object
                  (lambda (,machine-var ,self-var . ,arguments)
                    (declare (ignorable ,machine-var ,self-var))
                    ,@body)
                  :method-arguments
                  (let ((method-arguments-list
                          (method-arguments<-cl-lambda-list  ',arguments)))
                    (make-array (length method-arguments-list)
                                :initial-contents
                                method-arguments-list))))

(defmacro define-box-method (name (box-name &key
                                              (machine-var 'machine)
                                              (self-var 'self))
                             (&rest arguments)
                             &body body)

  `(add-primitive-method
    ',box-name ,name
    ,(lay-foreign-function-constructor machine-var self-var arguments body)))

;;; old shit
#+ (or)
(defmacro define-primitive (name (&key (machine-var 'machine)
                                       (self-var 'self))
                                    (&rest arguments)
                            &body body)
  `(defun ,name (,machine-var ,object-var ,@arguments)
     ,@body))

(defmethod o:foreign-method-call ((method standard-foreign-method)
                                  machine self &rest arguments)
  (apply (o:wrapped-object method)
         machine self arguments))

(defclass standard-foreign-method-slot (o:foreign-method-slot
                                        standard-slot-description)
  ())

(defun make-primitive-method-slot (&rest initargs)
  (apply #'make-instance 'standard-foreign-method-slot initargs))
