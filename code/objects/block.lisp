#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

;;; Smalltalk 80 has specific block creation instructions
;;; and so does a little smalltalk and this stops
;;; block objects ending up in literals.
;;; TODO do we know if we want block arguments.
;;; TODO what to do about protocol classes e.g. o:method
;;; TODO add block create method from template.

;;; wrap block template rather than have it inherit the method block?
(define-primitive block-template
    (:wrapped-slot-type method-block))

(define-box-method "create" (block-template) (lexical-context)
  (create-block self lexical-context))

(defun o:make-block-template (&rest initargs)
  (make-instance 'block-template
   :wrapped-object (apply #'make-instance 'method-block initargs)))

;;; the difference between a block and an activation record
;;; is that a block isn't an activation and has it's own activation
;;; the block is just an object that can be activated with some context
;;; of it's own.
;;; the activation object is the lexical parent
;;; we can just take the box context and put it in the machine though.
;;; then we would also have to define the create-block method
;;; on block-tempalate in the machine.
;;; the block activation can't share the same activation record because
;;; a method activation has a Self* parent slot
;;; and a block activation needs to refer to the Slef* parent
;;; by using the anonymous parent slot.

;;; TODO how is this going to interact with dynamic environment
;;; TODO we need a block object with this block-method-object
;;; as a method that can be activated when the value message is sent.
(defclass block-object (standard-object)
  ())


(defmethod create-block ((block-template block-template) lexical-parent)
  (let ((block-method-object (o:clone (o:wrapped-object block-template)))
        (lexical-context-slot
          (o:make-slot :slot-parent-p t
                       :slot-assignable-p nil
                       :slot-key +hidden-parent-selector+))
        (block-object (make-instance 'block-object))
        (value-slot
          (o:make-slot :slot-parent-p nil
                       :slot-assignable-p nil
                       :slot-key (o:internal-symbol "value"))))
    (o:add-slot block-method-object lexical-context-slot lexical-parent)
    (o:add-slot block-object value-slot block-method-object)
    block-object))
