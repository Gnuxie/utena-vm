;;;; package.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(defpackage #:utena-vm.objects
  (:use #:cl)
  (:shadow
   #:map
   #:symbol
   #:symbol-name
   #:make-array
   #:intern
   #:find-symbol
   #:method
   #:make-method)
  (:export
   #:object
   #:lookup-message
   #:add-slot
   #:make-object
   #:clone

   #:mapped-object
   #:object-ref
   #:object-map

   #:slot-description
   #:slot-key
   #:slot-parent-p
   #:slot-assignable-p
   #:data-slot-description
   #:slot-location
   #:add-slot
   #:make-slot
   #:data-slot-value

   #:assignment-slot-description
   #:target-slot-description
   #:add-assignment-slot

   #:map
   #:find-slot
   #:parent-slots

   #:box
   #:wrapped-object
   #:upgrade-box
   #:box-primitive

   #:symbol
   #:symbol-name

   #:make-array

   ;; symbol-table
   #:internal-symbol
   #:find-symbol
   #:intern
   #:*internal-symbol-table*

   #:method
   #:make-method
   #:method-code
   #:method-literals
   #:method-arguments

   #:foreign-method
   #:foreign-method-call
   #:foreign-method-slot

   #:block-object
   #:create-block
   #:block-template
   #:make-block-template))

(defpackage #:utena-vm.standard-objects
  (:use #:cl)
  (:shadow
   #:find-symbol
   #:make-symbol
   #:intern
   #:standard-object
   #:standard-method)
  (:local-nicknames
   (#:o #:utena-vm.objects)
   (#:u-vm.m #:utena-vm.machine))
  (:export
   #:define-primitive
   #:define-box-method

   #:standard-object
   #:method-block
))
