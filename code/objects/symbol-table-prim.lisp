#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

;;; the primtiives need to be in a seperate compilation unit
;;; because they need o:internal-symbol to be defined for the expansion
;;; of define-box-method to work

(define-box-method "find-symbol" (symbol-table) (string)
  (o:find-symbol self string))

(define-box-method "intern" (symbol-table) (string)
  (o:intern self string))

(define-box-method "symbol-name" (standard-symbol) ()
  (o:box-primitive (o:wrapped-object self)))
