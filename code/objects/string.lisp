#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(define-primitive primitive-character (:wrapped-slot-type character))

(defmethod o:box-primitive ((character cl:character))
  (make-instance 'primitive-character :wrapped-object character))

(define-primitive primitive-string (:wrapped-slot-type string))

(defmethod o:box-primitive ((string cl:string))
  (make-instance 'primitive-string :wrapped-object string))

(define-box-method "elt" (primitive-string) (index)
  (o:box-primitive (elt (o:wrapped-object self) index)))
