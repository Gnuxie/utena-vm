#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

;;; TODO
;;; Maybe we need an array factory for arrays with different dimensions?

;;; maybe we don't want adjustable arrays
;;; for example, it would help us a lot when creating bytecode programs
;;; if the bytecodes in methods were unboxed
;;; but then that means peopel can't create methods
;;; from within the VM which makes no sense?
(define-primitive primitive-array
    (:wrapped-slot-initform (make-array 3 :fill-pointer 0 :adjustable t)))

(define-box-method "aref" (primitive-array) (&rest elements)
  (apply #'aref (o:wrapped-object self) elements))

(define-box-method "aref:" (primitive-array) (new-element &rest elements)
  (setf (apply (o:wrapped-object self) elements)
        new-element))

(defmethod o:make-array (dimensions &key initial-contents)
  (if initial-contents
      (make-array dimensions :initial-contents initial-contents
                             :adjustable t)
      (make-array dimensions :fill-pointer 0 :adjustable t)))

(defmethod o:clone ((object primitive-array))
  (let ((clone (call-next-method)))
    (setf (o:wrapped-object clone)
          (alexandria:copy-array (o:wrapped-object clone)))
    clone))
