#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

(define-primitive primitive-number ())

(defmethod o:box-primitive ((number cl:number))
  (make-instance 'primitive-number :wrapped-object number))

(define-box-method "+" (primitive-number) (&rest numbers)
  (o:box-primitive (apply #'+ (o:wrapped-object self)
                          (mapcar #'o:wrapped-object numbers))))

(define-box-method "<" (primitive-number) (&rest numbers)
  (o:box-primitive (apply #'< (o:wrapped-object self)
                          (mapcar #'o:wrapped-object numbers))))

(define-box-method ">" (primitive-number) (&rest numbers)
  (o:box-primitive (apply #'> (o:wrapped-object self)
                          (mapcar #'o:wrapped-object numbers))))
