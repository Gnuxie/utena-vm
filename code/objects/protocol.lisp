#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.objects)

(defclass object () ())

(defgeneric lookup-message (object message-key)
  (:documentation "Lookup a message and return
a slot, do not evaluate the message."))

(defgeneric add-slot (object slot-description value)
  (:documentation "Add a slot to the object and associate it with the provided
value."))

(defgeneric make-object (&rest initargs &key &allow-other-keys))

(defgeneric clone (object)
  (:documentation "called in order to make a shallow copy of the object."))

(defclass mapped-object (object) ()
  (:documentation "A mapped object is an object that has an object map associated with it."))

(defgeneric object-ref (object ref)
  (:documentation "Access the slot at the location"))
(defgeneric (setf object-ref) (new-value object ref))

(defgeneric object-map (object)
  (:documentation "Return the map showing the slot-locations"))

(defgeneric (setf object-map) (new-value object))

(defclass slot-description () ()
  (:documentation "Protocol class for all slot descriptions."))

(defgeneric slot-key (slot-description)
  (:documentation "The key that is associated with the slot,
usually a symbol. Key is synonymous with selector."))

(defgeneric slot-assignable-p (slot-description)
  (:documentation "Whether the slot is assignable
really this is whether its allocation is on the object or the map."))

(defgeneric slot-parent-p (slot-description)
  (:documentation "True if the parent slot is a parent slot.

See parent-slots"))

(defclass data-slot-description (slot-description) ()
  (:documentation "When a data slot is evaluated, the value
associated with it will be placed onto the exectuion stack."))

(defgeneric slot-location (slot-description)
  (:documentation "The location of the slot on the map/object vector."))

(defgeneric (setf slot-location) (new slot-description))

(defgeneric make-slot (&rest initargs &key &allow-other-keys))

(defun data-slot-value (slot-description object)
  "Retrive the value associated with the data slot description
from the object"
  (if (slot-assignable-p slot-description)
      (object-ref object (slot-location slot-description))
      (slot-location slot-description)))

(defun (setf data-slot-value) (new-value slot-description object)
  (assert (slot-assignable-p slot-description) ()
          "Cannot set the value of a slot that is not assignable")
  (setf (object-ref object (slot-location slot-description))
        new-value))

(defclass assignment-slot-description (slot-description) ()
  (:documentation "protocol class for the assignment slot.
The assignment slot is used to provide a standard message for setting
the value of a data slot.

See data-slot-description"))

(defgeneric target-slot-description (assignment-slot-description)
  (:documentation "Returns the data-slot that the assignment slot is
targetting"))

(defgeneric add-assignment-slot (object target-slot-description selector)
  (:documentation "Add an assignment slot targetting this slot description
to the object."))

(defclass map () ())

(defgeneric find-slot (map key-object))

(defgeneric parent-slots (map)
  (:documentation "Returns a list of slots from the map which are parent slots.

See parent-slot-p"))

(defclass box (object) ())

(defgeneric wrapped-object (box)
  (:documentation "Return the wrapped primitive object"))

(defgeneric upgrade-box (box)
  (:documentation "Upgrade the box to a mapped-object
This is so that Uniform Reference Semantics can be maintained
and so a primitive can have annotations added to it like any other object

The upgraded box does not have to be a box, it only needs to be an object"))

(defgeneric box-primitive (primitive)
  (:documentation "Box the primitive so it can be presented to the guest environment. "))

(defgeneric make-array (dimensions &key initial-contents))

;;; methods
(defclass method () ())

(defgeneric method-code (method)
  (:documentation "A fixed array of unboxed bytecodes"))

(defgeneric method-literals (method)
  (:documentation "An unboxed vector of boxed literals"))

(defgeneric variable-arguments-p (method)
  (:documentation "Whether a caller can supply a varying number
of arguments"))

(defgeneric static-argument-count (method)
  (:documentation "The number of static arguments the method
will take of off the execution-stack"))

;;; primitive method

(defclass foreign-method () ())

(defgeneric foreign-method-call (foreign-method machine self &rest arguments))

(defclass foreign-method-slot () ())
