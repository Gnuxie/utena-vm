#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.standard-objects)

;;; we should keep an boxed version and an unboxed version
;;; of the code
;;; just so it's cached
;;; i'm starting to see why primtives are so unfriendly
;;; well actually, we can just say that initializing
;;; the method 'copies' the primtive values
;;; and then that can get around this problem.
(defclass method-block (o:method standard-object)
  ((%code :initarg :method-code :reader o:method-code)
   (%literals :initarg :method-literals :reader o:method-literals)
   (%arguments :initarg :method-arguments :reader o:method-arguments
               :initform (make-array 3 :adjustable t :fill-pointer 0)
               :documentation "A vector of argument slots in the order
they should be removed from the execution stack.")))

(defmethod o:clone ((method-block method-block))
  (make-instance 'method-block
                 :object-map (%object-map method-block)
                 :object-vector (alexandria:copy-array (object-vector method-block))
                 :method-code (o:method-code method-block)
                 :method-literals (o:method-literals method-block)
                 :method-arguments (o:method-arguments method-block)))

(defclass standard-argument-slot (standard-slot-description)
  ((%slot-assignable-p :allocation :class :initform t)
   (%argument-position :initarg :argument-position :reader argument-position
                       :type unsigned-byte :documentation "the position
of the argument when popping from the execution stack of the machine.")))

(defclass direct-argument-slot (standard-argument-slot) ())

(defun make-argument-slot (&rest initargs)
  (apply #'make-instance 'direct-argument-slot initargs))

(defclass rest-argument-slot (standard-argument-slot) ())

(defun make-rest-argument-slot (&rest initargs)
  (apply #'make-instance 'rest-argument-slot initargs))

(defmethod o:add-slot :around ((slot standard-argument-slot) (object method-block) value)
  (declare (ignore value))
  (vector-push-extend (slot-value object '%arguments)
                      slot)
  (call-next-method))
