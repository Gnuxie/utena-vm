(ql:register-local-projects)
(ql:quickload :staple-markdown)
(ql:quickload :utena-vm)

(defun strip-to-relative (some-path-string)
  (let ((chop-point (search "utena-vm" some-path-string)))
    (format *standard-output* "~a~%" (subseq some-path-string (+ chop-point 9)))
    (subseq some-path-string (+ chop-point 9))))

(defun format-source-for-repo (source)
  (format NIL "http://gitlab.com/Gnuxie/utena-vm/blob/master/~a#L~a"

          (strip-to-relative (namestring (getf source :file)))
    (getf source :row)))

(defmethod staple:resolve-source-link (source (page staple:definitions-index-page))
  (format-source-for-repo source))

(defmethod staple:resolve-source-link (source (page staple:system-page))
  (format-source-for-repo source))

(staple:generate
 :utena-vm
 :packages '(:utena-vm.objects
             :utena-vm.standard-objects
             :utena-vm.machine
             :utena-vm.standard-machine)
 :if-exists :supersede
 :output-directory (asdf:system-relative-pathname :utena-vm "documentation/"))
