;;;; utena-vm.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:utena-vm
  :description "Describe utena-vm here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "COOPERATIVE SOFTWARE LICENSE"
  :version "0.0.1"
  :serial t
  :depends-on ("utena-vm.machine" "utena-vm.objects"))
