#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(ql:register-local-projects)
(ql:update-dist "quicklisp" :prompt nil)
(ql:quickload :utena-vm.test)
(ql:quickload :utena-vm.test.objects)
(ql:quickload :utena-vm.test.machine)
(utena-vm.test:ci-run)
