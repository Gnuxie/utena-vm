#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:utena-vm.test
  (:use #:cl)
  (:local-nicknames (#:p #:parachute))
  (:export
   #:run
   #:ci-run
   #:utena-vm.test))

(in-package #:utena-vm.test)

(p:define-test utena-vm.test)

(defun run (&key (report 'p:plain))
  (p:test 'utena-vm.test :report report))

(defun ci-run ()
  (let ((test-result (run)))
    (unless (null (p:results-with-status :failed test-result))
      (uiop:quit -1))))
