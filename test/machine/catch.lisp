#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.machine)

(defvar *catch-prototype* (make-point-prototype))

(defvar *simple-escape* nil)
(define-test-method *simple-escape* ()
    ("x" "y")
  ;; put the value x on the stack
  (sm:+op-literal+ 0)
  ;; put the AR on the stack
  (sm:+op-activation+ 0)
  ;; return to the caller with x
  (sm:+op-escape+ 0)
  ;; put the value y on the stack if we didn't escape so we can detect that
  ;; but this should be 'unreachable' code
  (sm:+op-literal+ 1))

(ensure-test-method-on-object *simple-escape*
                              *catch-prototype*
                              (o:internal-symbol "simple-escape"))

(defvar *call-simple-escape* nil)
(define-test-method *call-simple-escape* ()
    (*catch-prototype* "simple-escape")
  (sm:+op-literal+ 0)
  (sm:+op-send+ 1))

(p:define-test simple-escape
  :parent utena-vm.test.machine

  (let ((machine (sm:make-machine)))
    (sm:evaluate-activation-record machine
                                   (sm:activate machine
                                                *catch-prototype*
                                                *call-simple-escape*)
                                   (o:method-code *call-simple-escape*))
    (p:is #'eq (o:internal-symbol "x")
          (m:pop-execution-stack machine))))

(defun make-looping-prototype ()
  (let* ((data-descriptions
           (list
            (o:make-slot :slot-key (o:internal-symbol "i")
                         :slot-assignable-p t)
            (o:make-slot :slot-key (o:internal-symbol "escape-block")
                         :slot-assignable-p t)
            (o:make-slot :slot-key (o:internal-symbol "escape-record")
                         :slot-assignable-p t)))
         (object
           (o:make-object
            :slot-descriptions data-descriptions
            :initial-values
            (list (o:box-primitive 1) (o:box-primitive nil) (o:box-primitive nil)))))
    (o:add-assignment-slot object (nth 0 data-descriptions) (o:internal-symbol "i:"))
    (o:add-assignment-slot object (nth 1 data-descriptions) (o:internal-symbol "escape-block:"))
    (o:add-assignment-slot object (nth 2 data-descriptions) (o:internal-symbol "escape-record:"))
    object))

(defvar *escaping-block* nil)
(define-test-method *escaping-block* (o:make-block-template)
    ("escape-record")
  (sm:+op-implicit-self-send+ 0)
  (sm:+op-escape+ 0))

(defvar *simple-reset-block* nil)
(define-test-method *simple-reset-block* (o:make-block-template)
    ("i" "i:" ">" 5 "if-true" "escape-block" 1 "+" "escape-record:")
  ;; put the AR onto the stack
  (sm:+op-activation+ 0)
  ;; give the AR to escape-record
  (sm:+op-implicit-self-send+ 8)
  ;; put the escape block onto the stack
  (sm:+op-implicit-self-send+ 5)
  ;; put the value 5 on the stack
  (sm:+op-literal+ 3)
  ;; put the value of x on the stack
  (sm:+op-implicit-self-send+ 0)
  ;; set arg count register to 1
  (sm:+op-arg-count+ 1)
  ;; send > to the value of i and compare against 5
  ;; if i > 5:
  (sm:+op-send+ 2)
  ;; ifTrue, use the escape block
  (sm:+op-send+ 4)
  ;; set i to i + 1 by putting i down first
  (sm:+op-implicit-self-send+ 0)
  ;; put the 1 on the stack
  (sm:+op-literal+ 6)
  ;; send + to the 1 and i
  (sm:+op-send+ 7)
  ;; set i to the result
  (sm:+op-implicit-self-send+ 1)
  ;; put the AR onto the atck
  (sm:+op-activation+ 0)
  ;; reset
  (sm:+op-reset+ 0))

(defvar *simple-reset-method* nil)
(define-test-method *simple-reset-method* ()
    (*catch-prototype* 0 5 "i" "i:" *escaping-block* "create" "escape-block:"
     *simple-reset-block* "value")
  ;; push 0 onto the stack
  (sm:+op-literal+ 1)
  ;; set i to 0
  (sm:+op-implicit-self-send+ 4)
  ;; put the lexical context that we want the escaping block to have
  ;; onto the stack
  (sm:+op-activation+ 0)
  ;; get the escaping block literal and create it
  (sm:+op-literal+ 5)
  ;; send the create method with the block and the lexical context
  (sm:+op-send+ 6)
  ;; set up escape-block to hold the created escpae block
  (sm:+op-implicit-self-send+ 7)
  ;; setup the simple reset block
  (sm:+op-activation+ 0)
  ;; put the simple reset block template onto the stack
  (sm:+op-literal+ 8)
  ;; send create to the block template
  (sm:+op-send+ 6)
  ;; send value to the created block template
  (sm:+op-send+ 9)
  ;; put i back onto the execution stack
  (sm:+op-implicit-self-send+ 3))

(p:define-test catch-block
  :parent utena-vm.test.machine

  (let ((machine (sm:make-machine))
        (looping-object (make-looping-prototype)))
    (sm:evaluate-activation-record
     machine
     (sm:activate machine looping-object
                  *simple-reset-method*)
     (o:method-code *simple-reset-method*))
    (p:is #'= 6 (o:wrapped-object (m:pop-execution-stack machine)))
    (p:is #'= 6 (o:wrapped-object
                 (o:object-ref looping-object
                               (o:slot-location (o:lookup-message looping-object (o:internal-symbol "i"))))))))
