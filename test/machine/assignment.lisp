#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.machine)

(defparameter *assignment-test-prototype*
  (make-point-prototype))

(defvar *assign-point-value* nil)
(define-test-method *assign-point-value* ()
    ("x" "x:" 5 *assignment-test-prototype*)
  ;; push the number 5
  (sm:+op-literal+ 2)
  ;; push the prototype object
  (sm:+op-literal+ 3)
  ;; send x: to the prototpye object with the value 5
  (sm:+op-send+ 1)
  ;; put the protoype back
  (sm:+op-literal+ 3)
  ;; send x
  (sm:+op-send+ 0))

(p:define-test assignment
  :parent utena-vm.test.machine

  (let* ((machine (sm:make-machine)))
    (sm:evaluate-activation-record
     machine
     (sm:activate machine
                  *assignment-test-prototype*
                  *assign-point-value*)
     (o:method-code *assign-point-value*))
    (let ((boxed-five (m:pop-execution-stack machine)))
      (p:is #'= 5 (o:wrapped-object boxed-five)))))
