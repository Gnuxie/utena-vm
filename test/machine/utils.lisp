#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.machine)

(defun make-method-code (instruction-specs)
  (make-array (length instruction-specs)
              :initial-contents
              (loop :for (opcode operand) :in instruction-specs
                    :collect (sm:make-instruction opcode operand))))

(defun make-method-literals (literals)
  (make-array (length literals)
              :initial-contents
              (loop :for literal :in literals
                    :collect (typecase literal
                               (string (o:intern o:*internal-symbol-table* literal ))
                               (list literal)
                               (o:object literal)
                               (t (o:box-primitive literal))))))

(defun make-method-arguments (arguments)
  (make-array (length arguments)
              :initial-contents
              (loop :for argument :in arguments
                    :for i :from 0 :upto (length arguments)
                    :collect (so::make-argument-slot :slot-assignable-p nil
                                                     :argument-position i
                                                     :slot-key (o:internal-symbol argument)))))

;;; maybe we will need soemthing like this later
#+(or)
(defun make-method-arguments (arguments)
  (loop :named argument-builder
        :for symbol :in arguments
        :do (o:make-argument-slot
             :slot-key (o:internal-symbol symbol))))

(defmacro define-test-method (place (&optional (method-constructor
                                                'o:make-method))
                              (&rest literals) &body instructions)
  `(setf ,place
         (,method-constructor
          :method-code
          (make-method-code
           ,`(list
              ,@
              (loop :for entry :in instructions
                    :collect `(list ,(first entry) ,(second entry)))))
          :method-literals
          (make-method-literals
           ,`(list ,@literals)))))

(defmacro define-method (object-place method-name (&rest method-arguments)
                         (&rest method-literals) &body method-code)
  `(ensure-test-method-on-object
    (o:make-method
     :method-code
     ,`(list
        ,@ (loop :For entry :in method-code
                 :collect `(list ,(first entry) ,(second entry))))
     :method-literals
     (make-method-literals
      ',method-literals)
     :method-arguments
     (make-method-arguments ',method-arguments))
    ,object-place
    ',method-name))

(defun ensure-test-method-on-object (method object selector)
  (o:add-slot object
              (o:make-slot :slot-key selector
                           :slot-assignable-p nil
                           :slot-location 0)
              method))
