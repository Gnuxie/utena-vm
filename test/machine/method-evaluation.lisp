#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.machine)

(defun make-point-prototype ()
  (let* ((data-descriptions
           (list
            (o:make-slot :slot-key (o:internal-symbol "x")
                             :slot-assignable-p t)
            (o:make-slot :slot-key (o:internal-symbol "y")
                             :slot-assignable-p t)))
         (object
           (o:make-object
            :slot-descriptions data-descriptions
            :initial-values
            (list (o:box-primitive 1) (o:box-primitive 2)))))
    (o:add-assignment-slot object (nth 0 data-descriptions) (o:internal-symbol "x:"))
    (o:add-assignment-slot object (nth 1 data-descriptions) (o:internal-symbol "y:"))
    object))

(defvar *add-point-value* nil)
(defvar *test-prototype* (make-point-prototype))

(define-test-method *add-point-value* ()
    (*test-prototype*
     "+"
     "x"
     "y")
  ;; set the point to be the reciever of the message 'x'
  (sm:+op-literal+ 0)
  ;; send message 'x' to the point and put the result on the stack
  (sm:+op-send+ 2)
  ;; put the point back on the stack so we can get y from it
  (sm:+op-literal+ 0)
  ;; evalute y from the prototype, putting it on the stack, which becomes the reciever
  (sm:+op-send+ 3)
  ;; set arg count register to 1
  (sm:+op-arg-count+ 1)
  ;; send the + message
  (sm:+op-send+ 1))

;;; we should use the point maker from the other package
;;; and test adding two of the poitns together
;;; we could even go as far as assigning the result to something..
;;; like another slot using the assingment primitive.
(p:define-test simple-method-evaluation
  :description "Evaluate a method which does not have any reference to
the implicit self."
  :parent utena-vm.test.machine

  (let* ((machine (sm:make-machine))
         (test-activation (sm:activate machine *test-prototype* *add-point-value*)))
    (sm:evaluate-activation-record machine test-activation (o:method-code *add-point-value*))
    (p:is #'= 3 (o:wrapped-object (m:pop-execution-stack machine))
          "The result of adding 1 and 2 from the prototype slots should be 3.")))

(defvar *method-into-method-method* nil)
(defvar *method-into-method-object*
  (make-point-prototype))

(define-test-method *method-into-method-method* ()
    ("test"
     *method-into-method-object*)
  ;; put our object with it's method onto the stack
  (sm:+op-literal+ 1)
  ;; call the method
  (sm:+op-send+ 0))

(ensure-test-method-on-object *add-point-value*
                              *method-into-method-object*
                              (o:internal-symbol "test"))

(p:define-test method-into-method
  :description "This test should test sending a message
to a slot who's value is a method and testing the calling semantics

TODO
We have no idea what the calling semantics are yet, but this method
does work."
  :parent utena-vm.test.machine

  (let* ((machine (sm:make-machine))
         (test-activation
           (sm:activate machine
                        *method-into-method-object*
                        *method-into-method-method*)))
    (sm:evaluate-activation-record machine
                                   test-activation
                                   (o:method-code *method-into-method-method*))
    (m:pop-execution-stack machine)))
