#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:utena-vm.test.machine
  (:use #:cl)
  (:local-nicknames
   (#:p #:parachute)
   (#:o #:utena-vm.objects)
   (#:m #:utena-vm.machine)
   (#:sm #:utena-vm.standard-machine)
   (#:to #:utena-vm.test.objects)
   (#:so #:utena-vm.standard-objects))
  (:export
   #:utena-vm.test.machine
   #:define-test-method))

(in-package #:utena-vm.test.machine)

(p:define-test utena-vm.test.machine
  :parent (#:utena-vm.test #:utena-vm.test))
