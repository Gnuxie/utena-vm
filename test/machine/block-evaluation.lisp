#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.machine)

(defvar *give-x* nil)

(define-test-method *give-x* (o:make-block-template)
    ("x")
  ;; simpley put this literal on the execution stack as the method.
  (sm:+op-literal+ 0))

(defvar *return-block* nil)

(define-test-method *return-block* ()
    (*test-prototype*
     *give-x*
     "create")
  ;; push the lexical context to give to the create method of *give-x*
  ;; this should actually be an activiation record?
  (sm::+op-activation+ 0)
  ;; get the template to create a block
  (sm:+op-literal+ 1)
  ;; send the create message and thus return a new block.
  (sm:+op-send+ 2))

(defvar *method-into-block-test* (make-point-prototype))

(ensure-test-method-on-object *return-block*
                              *method-into-block-test*
                              (o:internal-symbol "return-x-block"))

(defvar *method-into-block-method* nil)
(define-test-method *method-into-block-method* ()
    (*method-into-block-test*
     "return-x-block"
     "value")
  ;; put our test object on the stack
  (sm:+op-literal+ 0)
  ;; send return-x-block and have that block on the stack
  (sm:+op-send+ 1)
  ;; call value on that block, which should leave the symbol x on the stack
  (sm:+op-send+ 2))

(p:define-test block-evaluation
  :parent utena-vm.test.machine)

(p:define-test block-creation
  :parent block-evaluation

  (let* ((machine (sm:make-machine))
         (return-block-activation
           (sm:activate machine
                        *test-prototype*
                        *return-block*)))
    (sm:evaluate-activation-record machine
                                   return-block-activation
                                   (o:method-code *return-block*))
    (let ((x-block (m:pop-execution-stack machine)))
      (p:true (typep x-block 'so::block-object)))))

(p:define-test block-activation
  :parent block-evaluation

  (let* ((machine (sm:make-machine))
         (method-into-block-activation
           (sm:activate machine
                        *method-into-block-test*
                        *method-into-block-method*)))
    (sm:evaluate-activation-record machine
                                   method-into-block-activation
                                   (o:method-code *method-into-block-method*))
    (let ((x-symbol (m:pop-execution-stack machine)))
      (p:is #'string= "x" (o:wrapped-object x-symbol)))))

(defvar *if-true-method* nil)
(define-test-method *if-true-method* ()
    ("if-true"
     t
     *give-x*
     "create")
  ;; put down the context for the block
  (sm:+op-activation+ 0)
  ;; put the block template to execute if true
  (sm:+op-literal+ 2)
  ;; create the block
  (sm:+op-send+ 3)
  ;; put true value on the execution stack
  (sm:+op-literal+ 1)
  ;; send the if-true message to the true value
  (sm:+op-send+ 0))

(p:define-test boolean-evaluation
  :parent block-evaluation

  (let* ((machine (sm:make-machine))
         (activation
           (sm:activate machine
                        *method-into-block-test*
                        *if-true-method*)))
    (sm:evaluate-activation-record machine
                                   activation
                                   (o:method-code *if-true-method*))
    (let ((x-symbol (m:pop-execution-stack machine)))
      (p:is #'string= "x" (o:wrapped-object x-symbol)))))
