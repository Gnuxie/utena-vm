(asdf:defsystem #:utena-vm.test.machine
  :description "test system for the utena-vm.standard-machine sytsem"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :depends-on ("utena-vm.machine" "utena-vm.test.objects" "utena-vm.test" "parachute")
  :serial t
  :components ((:file "package")
               (:file "utils")
               (:file "method-evaluation")
               (:file "assignment")
               (:file "block-evaluation")
               (:file "catch")))
