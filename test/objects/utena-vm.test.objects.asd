(asdf:defsystem #:utena-vm.test.objects
  :description "test system for the utena-vm.objects sytsem"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :depends-on ("utena-vm.objects" "utena-vm.test" "parachute")
  :serial t
  :components ((:file "package")
               (:file "symbol-table")
               (:file "transition")
               (:file "boxed-primitives")))
