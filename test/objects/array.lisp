#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.objects)

;;; To test methods we normally have to use the machine
;;; since they're the only things that can evaluate messages
;;; so we should either make a test machine or bodge the evalutation
;;; by getting the fucntion out of the method.
