#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.objects)

(p:define-test box-map-lookup
  :parent utena-vm.test.objects
  :description "Test basic lookup for boxes"

  (let ((boxed-string (o:box-primitive "test")))
    (let ((element-method-slot
            (o:find-slot (o:object-map boxed-string)
                         (o:internal-symbol "elt"))))
      (p:true element-method-slot "Should be able to find the element
slot from the boxed string."))
    (let ((element-method
            (o:data-slot-value (o:lookup-message boxed-string (o:internal-symbol "elt"))
                               boxed-string)))
      (p:true element-method "Should have returned the element method.")
      (p:of-type function (o:wrapped-object element-method)))))

;;; TODO
;;; we need to check the type of the arguments that we have
;;; e.g. that they're a rest argument slot etc.
(p:define-test box-map-arguments
  :parent utena-vm.test.objects
  :description "Test that argument lists are generated for primitive methods"

  (let ((symbol-table o:*internal-symbol-table*))
    (let ((intern-method-slot
            (o:find-slot (o:object-map symbol-table)
                         (o:internal-symbol "intern"))))
      (p:true intern-method-slot "Should be able to find the intern method.")
      (let* ((intern-primitive-method (o:data-slot-value intern-method-slot symbol-table))
             (intern-arguments (o:method-arguments intern-primitive-method)))
        (p:is = 1 (length intern-arguments) "Should be one argument which is the string
to intern")
        (p:true (o:slot-key (aref intern-arguments 0))))))

  (let* ((number (o:box-primitive 3))
         (add-slot (o:find-slot (o:object-map number)
                                (o:internal-symbol "+")))
         (add-method (o:data-slot-value add-slot number))
         (add-arguments (o:method-arguments add-method)))
    (p:is = 1 (length add-arguments))))
