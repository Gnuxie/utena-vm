#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.objects)

(p:define-test symbol-table
  :parent utena-vm.test.objects
  :description "Test that symbols can be found and interned in the internal
symbol table."
  (let ((symbol-1-string (symbol-name (gensym)))
        (symbol-2-string (symbol-name (gensym))))
    (p:true (null (o:find-symbol o:*internal-symbol-table* symbol-1-string)))
    (let ((symbol-1 (o:internal-symbol symbol-1-string))
          (symbol-2 (o:intern o:*internal-symbol-table* symbol-2-string)))
      (p:is #'eq symbol-1 (o:find-symbol o:*internal-symbol-table* symbol-1-string ))
      (p:is #'eq symbol-2 (o:find-symbol o:*internal-symbol-table* symbol-2-string)))))
