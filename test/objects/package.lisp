#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:utena-vm.test.objects
  (:use #:cl)
  (:local-nicknames
   (#:p #:parachute)
   (#:o #:utena-vm.objects)
   (#:so #:utena-vm.standard-objects))
  (:export
   #:utena-vm.test.objects
   #:make-point-prototype))

(in-package #:utena-vm.test.objects)

(p:define-test utena-vm.test.objects
  :parent (#:utena-vm.test #:utena-vm.test))
