#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:utena-vm.test.objects)

(defun make-point-prototype ()
  (o:make-object
   :slot-descriptions
   (list
    (o:make-slot :slot-key (o:internal-symbol "x")
                 :slot-assignable-p t)
    (o:make-slot :slot-key (o:internal-symbol "y")
                 :slot-assignable-p t))
   :initial-values
   '(1 2)))

(p:define-test map-lookup
  :parent utena-vm.test.objects
  :description "Test message lookup on mapped objects"

  (let ((point-prototype (make-point-prototype)))
    (let ((x-slot (o:find-slot (o:object-map point-prototype)
                               (o:internal-symbol "x"))))
      (p:true x-slot "Should be able to find the slot on the map."))
    (p:is #'= 1
          (o:data-slot-value
           (o:lookup-message point-prototype
                             (o:internal-symbol "x"))
           point-prototype)
          "Should be able to read slot contents.")))

(p:define-test transition
  :parent utena-vm.test.objects
  :description "Test that a transition tree is created and used when
adding slots in order. "

  (let ((point-prototype (make-point-prototype))
        (next-slot
          (o:make-slot
           :slot-key (o:internal-symbol "z")
           :slot-assignable-p t)))
    (let ((clone1 (o:clone point-prototype))
          (clone2 (o:clone point-prototype)))
      (p:is #'eq (o:object-map clone1) (o:object-map clone2)
            "unmodified clones should share the same object map")
      (let ((old-map (o:object-map clone1)))
        (o:add-slot clone1 next-slot 3)
        (o:add-slot clone2 next-slot 4)
        (p:false (eq old-map (o:object-map clone1))
                 "The map should have changed after adding a new slot.")
        (p:is #'eq (o:object-map clone1) (o:object-map clone2)
              "clones modified with the same slot-descriptor in the same order
should share the same object map")))))

(p:define-test parent-lookup
  :parent map-lookup
  :description "Slots on parent objects are correctly dealt with."

  (let ((tratial-object
          (o:make-object
           :slot-descriptions
           (list (o:make-slot :slot-key (o:internal-symbol "parent-message")
                         :slot-assignable-p nil))
           :initial-values '(5)))
        (a-point (make-point-prototype)))
    (o:add-slot a-point
                (o:make-slot :slot-parent-p t
                             :slot-key (o:internal-symbol "parent*")
                             :slot-assignable-p nil)
                tratial-object)
    (p:is #'eq (o:internal-symbol "parent-message")
          (o:slot-key (o:lookup-message a-point (o:internal-symbol "parent-message"))))))
