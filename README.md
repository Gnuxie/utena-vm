# UPDATE 2021-12-21

This was a project I did as a part of my bachelor's degree in computer so called science.
It was always going to be something beyond that I just used the degree part as an excuse to do it.

The project continues here https://gitlab.com/cal-coop/utena because what is here is kind of bad, I will probably explain why one day but not today.

What is here has a [report](https://applied-langua.ge/~gnuxie/documents/utena-vm.pdf) about it but some things are really wrong in here and the directions i suggest for the language including "roses" just don't work cos i tried them lwl. One day I will write about the differences between the new Utena and why they exist.


# utena-vm

This solution is written in Common Lisp.

To 'run' it you will need a Common Lisp implementation that quicklisp installed.

The easiest way to do that is to use [Portacle](https://portacle.github.io/) (unless you're on Mac in which case it will struggle because of the number of binaries and security features)

You will then need to copy this project to your quicklisp local-projects (in portable this is in the 'projects' folder)

Once you're in a repl, the project an be loaded with `(ql:register-local-projects)` and then `(ql:quickload :utena-vm)`.

The tests can be run by loading `(ql:quickload :utena-vm.test.machine)` and then running `(utena-vm.test:run)`.

There is some documentation generated from the docstrings that can be viewed [here](https://gnuxie.gitlab.io/utena-vm/utena-vm.machine/index.html).

## getting started

To get started you could make a point prototype like this: (Assumes that the package-local-nickname `o` is set to `utena-vm.objects` and `m` is set to `utena-vm.machine`)

```lisp
=> (defun make-point-prototype (x y)
  (let* ((data-descriptions
           (list
            (o:make-slot :slot-key (o:internal-symbol "x")
                             :slot-assignable-p t)
            (o:make-slot :slot-key (o:internal-symbol "y")
                             :slot-assignable-p t)))
         (object
           (o:make-object
            :slot-descriptions data-descriptions
            :initial-values
            (list (o:box-primitive x) (o:box-primitive y)))))
    (o:add-assignment-slot object (nth 0 data-descriptions) (o:internal-symbol "x:"))
    (o:add-assignment-slot object (nth 1 data-descriptions) (o:internal-symbol "y:"))
    object))

MAKE-POINT-PROTOTYPE
=> (make-point-prototype* 3 4)
#<SO:STANDARD-OBJECT {1003963F83}>
=> (o:wrapped-object (m:send-message * (o:internal-symbol "x") (utena-vm.standard-machine:make-machine)))
3
=> (o:wrapped-object (utena-vm.machine:send-message ** (o:internal-symbol "x:") (utena-vm.standard-machine:make-machine) (o:box-primitive 10)))
10

```

See the tests for more examples.
